<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>an-js_21JAN18</title>
	<script src="angular.min.js"></script>
</head>
<body>
<div ng-app="">
  <p>Name : <input type="text" ng-model="name"></p>
<?php for($i = 0;$i<5;$i++):?>
  <p>Hello {{name}}</p>
  <?php endfor?>
</div>
 <div ng-app="myApp" ng-controller="myCtrl">

First Name: <input type="text" ng-model="firstName"><br>
Last Name: <input type="text" ng-model="lastName"><br>
<br>
Full Name: {{firstName + " " + lastName}}

</div>

<script>
var app = angular.module('myApp', []);
app.controller('myCtrl', function($scope) {
    $scope.firstName= "John";
    $scope.lastName= "Doe";
});
</script> 
</body>
</html>